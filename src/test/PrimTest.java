package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import arbolGeneradorMinimo.Prim;
import grafos.GenerarGrafos;
import grafos.GrafoConPesos;

public class PrimTest {

	private GrafoConPesos grafo;
	private GrafoConPesos esperado;

	@Before
	public void iniciar() {
		grafo = new GrafoConPesos();
		esperado = new GrafoConPesos();
	}

	@Test(expected = IllegalArgumentException.class)
	public void grafoNulo() {
		Prim prim = new Prim(null);
		prim.arbolGeneradorMinimo();
	}

	@Test
	public void grafoVacio() {
		Prim prim = new Prim(grafo);
		prim.arbolGeneradorMinimo();

		assertEquals(esperado, grafo);
	}

	@Test
	public void arbolGeneradorMinimoPrimerTest() {
		grafo = GenerarGrafos.generarGrafo1();
		esperado = GenerarGrafos.resultadoEsperadoGrafo1();
		Prim prim = new Prim(grafo);

		assertEquals(esperado, prim.arbolGeneradorMinimo());
	}

	@Test
	public void arbolGeneradorMinimoSegundoTest() {
		grafo = GenerarGrafos.generarGrafo2();
		esperado = GenerarGrafos.resultadoEsperadoGrafo2();
		Prim prim = new Prim(grafo);

		assertEquals(esperado, prim.arbolGeneradorMinimo());
	}

	@Test
	public void arbolGeneradorMinimoTercerTest() {
		grafo = GenerarGrafos.generarGrafo3();
		esperado = GenerarGrafos.resultadoEsperadoGrafo3();
		Prim prim = new Prim(grafo);

		assertEquals(esperado, prim.arbolGeneradorMinimo());
	}

	@Test
	public void arbolGeneradorMinimoCuartoTest() {
		grafo = GenerarGrafos.generarGrafo4();
		esperado = GenerarGrafos.resultadoEsperadoGrafo4();
		Prim prim = new Prim(grafo);

		assertEquals(esperado, prim.arbolGeneradorMinimo());
	}

	@Test
	public void arbolGeneradorMinimoQuintoTest() {
		grafo = GenerarGrafos.generarGrafo5();
		esperado = GenerarGrafos.resultadoEsperadoGrafo5();
		Prim prim = new Prim(grafo);

		assertEquals(esperado, prim.arbolGeneradorMinimo());
	}
}