package test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import arbolGeneradorMinimo.BFS;
import grafos.Grafo;
import grafos.GrafoConPesos;

public class BFSTest {

	private GrafoConPesos grafo;

	@Before
	public void iniciar() {
		grafo = new GrafoConPesos(5);
		generarGrafo();
	}

	@Test(expected = IllegalArgumentException.class)
	public void grafoNuloTest() {
		BFS.esConexo(null);
	}

	@Test
	public void grafoVacioTest() {
		Grafo g = new Grafo(0);
		assertTrue(BFS.esConexo(g));
	}

	@Test
	public void grafoNoConexoTest() {
		assertFalse(BFS.esConexo(grafo));
	}

	@Test
	public void grafoConexoTest() {
		grafo.agregarArista(3, 4, 0);
		assertTrue(BFS.esConexo(grafo));
	}

	@Test
	public void alcanzables() {
		Set<Integer> esperado = new HashSet<>();
		esperado.add(0);
		esperado.add(1);
		esperado.add(2);
		esperado.add(3);

		assertEquals(esperado, BFS.alcanzables(grafo, 0));
	}

	@Test
	public void alcanzablesUnicoVertice() {
		Set<Integer> esperado = new HashSet<>();
		esperado.add(4);

		assertEquals(esperado, BFS.alcanzables(grafo, 4));
	}

	@Test(expected = IllegalArgumentException.class)
	public void alcanzablesGrafoNull() {
		BFS.alcanzables(null, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void alcanzablesVerticeInvalido() {
		BFS.alcanzables(grafo, 7);
	}

	@Test(expected = IllegalArgumentException.class)
	public void alcanzablesVerticeNegativo() {
		BFS.alcanzables(grafo, -1);
	}

	private void generarGrafo() {
		grafo.agregarArista(0, 2, 0);
		grafo.agregarArista(0, 1, 0);
		grafo.agregarArista(1, 3, 0);
	}
}