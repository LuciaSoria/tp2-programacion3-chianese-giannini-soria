package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import arbolGeneradorMinimo.AuxiliaresKruskal;
import grafos.Arista;
import grafos.GrafoConPesos;

public class AuxiliaresKruskalTest {

	private GrafoConPesos grafo;

	@Before
	public void iniciar() {
		grafo = new GrafoConPesos(6);
		generarGrafo();
	}

	@Test(expected = IllegalArgumentException.class)
	public void generarCicloGrafoNull() {
		Arista arista = new Arista(0, 1, 2);

		AuxiliaresKruskal.generaCiclo(null, arista);
	}

	@Test(expected = IllegalArgumentException.class)
	public void generarCicloAristaNull() {
		AuxiliaresKruskal.generaCiclo(grafo, null);
	}

	@Test
	public void generarCiclo() {
		Arista arista = new Arista(3, 2, 0);

		assertTrue(AuxiliaresKruskal.generaCiclo(grafo, arista));
	}

	@Test
	public void generarCicloFalse() {
		Arista arista = new Arista(2, 4, 0);

		assertFalse(AuxiliaresKruskal.generaCiclo(grafo, arista));
	}

	@Test(expected = IllegalArgumentException.class)
	public void generarCicloVerticeInvalido() {
		Arista arista = new Arista(2, 6, 0);

		AuxiliaresKruskal.generaCiclo(grafo, arista);
	}

	@Test(expected = IllegalArgumentException.class)
	public void generarCicloVerticeNegativo() {
		Arista arista = new Arista(2, -1, 0);

		AuxiliaresKruskal.generaCiclo(grafo, arista);
	}

	@Test(expected = IllegalArgumentException.class)
	public void aristaMinimaAristasNull() {
		AuxiliaresKruskal.elegirAristaMinima(null, grafo);
	}

	@Test(expected = IllegalArgumentException.class)
	public void aristaMinimaGrafoNull() {
		List<Arista> aristas = grafo.getAristas();
		AuxiliaresKruskal.elegirAristaMinima(aristas, null);
	}

	@Test
	public void aristaMinima() {
		List<Arista> aristas = new ArrayList<>();
		aristas.add(new Arista(3, 4, 2));
		aristas.add(new Arista(5, 4, 0));
		Arista resultado = AuxiliaresKruskal.elegirAristaMinima(aristas, grafo);
		Arista esperada = new Arista(5, 4, 0);

		assertEquals(esperada, resultado);
	}

	@Test
	public void aristaDeMenorPesoMismoPeso() {
		List<Arista> aristas = new ArrayList<>();
		aristas.add(new Arista(3, 4, 0));
		aristas.add(new Arista(5, 4, 0));
		Arista resultado = AuxiliaresKruskal.elegirAristaMinima(aristas, grafo);
		Arista esperada = new Arista(3, 4, 0);

		assertEquals(esperada, resultado);
	}

	@Test(expected = Exception.class)
	public void aristaDeMenorPesoVacia() {
		List<Arista> aristas = new ArrayList<>();
		AuxiliaresKruskal.elegirAristaMinima(aristas, grafo);
	}

	private void generarGrafo() {
		grafo.agregarArista(0, 2, 1);
		grafo.agregarArista(0, 1, 2);
		grafo.agregarArista(1, 3, 2);
	}
}