package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import arbolGeneradorMinimo.AuxiliaresPrim;
import grafos.Arista;
import grafos.GrafoConPesos;

public class AuxiliaresPrimTest {

	private GrafoConPesos grafo;
	private List<Integer> vertices;

	@Before
	public void iniciar() {
		grafo = crearGrafo();
		vertices = new ArrayList<>();
	}

	@Test(expected = Exception.class)
	public void aristasPosiblesGrafoNull() {
		vertices.add(0);
		vertices.add(1);
		vertices.add(2);
		AuxiliaresPrim.aristasPosibles(null, vertices);
	}

	@Test(expected = Exception.class)
	public void aristasPosiblesVerticesNull() {
		AuxiliaresPrim.aristasPosibles(grafo, null);
	}

	@Test
	public void aristasPosibles() {
		vertices.add(0);
		vertices.add(1);
		vertices.add(2);
		List<Arista> resultado = AuxiliaresPrim.aristasPosibles(grafo, vertices);
		List<Arista> esperado = esperado1();

		assertEquals(esperado, resultado);
	}

	@Test
	public void aristasPosiblesConUnVerticeRecorrido() {
		vertices.add(0);
		List<Arista> resultado = AuxiliaresPrim.aristasPosibles(grafo, vertices);
		List<Arista> esperado = esperadoUnVertice();

		assertEquals(esperado, resultado);
	}

	@Test
	public void aristasPosiblesSinVerticesRecorridos() {
		List<Arista> resultado = AuxiliaresPrim.aristasPosibles(grafo, vertices);
		List<Arista> esperado = new ArrayList<>();

		assertEquals(esperado, resultado);
	}

	@Test
	public void aristasPosiblesTodosLosVerticesRecorridos() {
		vertices.add(0);
		vertices.add(1);
		vertices.add(2);
		vertices.add(3);
		vertices.add(4);
		List<Arista> resultado = AuxiliaresPrim.aristasPosibles(grafo, vertices);
		List<Arista> esperado = new ArrayList<>();

		assertEquals(esperado, resultado);
	}

	@Test
	public void aristaDeMenorPeso() {
		vertices.add(0);
		vertices.add(1);
		List<Arista> aristas = AuxiliaresPrim.aristasPosibles(grafo, vertices);
		Arista resultado = AuxiliaresPrim.aristaDeMenorPeso(aristas);
		Arista esperada = new Arista(1, 2, 0);

		assertEquals(resultado, esperada);
	}

	@Test
	public void aristaDeMenorPesoMismoPeso() {
		grafo = grafoMismosPesos();
		Arista resultado = AuxiliaresPrim.aristaDeMenorPeso(grafo.getAristas());
		Arista esperada = new Arista(0, 1, 5);

		assertEquals(resultado, esperada);
	}

	@Test(expected = Exception.class)
	public void aristaDeMenorPesoNull() {
		AuxiliaresPrim.aristaDeMenorPeso(null);
	}

	@Test(expected = Exception.class)
	public void aristaDeMenorPesoVacia() {
		GrafoConPesos grafo = new GrafoConPesos(5);

		AuxiliaresPrim.aristaDeMenorPeso(grafo.getAristas());
	}

	private GrafoConPesos crearGrafo() {
		GrafoConPesos grafo = new GrafoConPesos(5);
		grafo.agregarArista(0, 1, 5);
		grafo.agregarArista(0, 4, 9);
		grafo.agregarArista(1, 2, 0);
		grafo.agregarArista(2, 3, 2);
		grafo.agregarArista(4, 3, 4);
		return grafo;
	}

	private List<Arista> esperado1() {
		List<Arista> esperado = new ArrayList<>();
		esperado.add(new Arista(2, 3, 2));
		esperado.add(new Arista(0, 4, 9));
		return esperado;
	}

	private List<Arista> esperadoUnVertice() {
		List<Arista> esperado = new ArrayList<>();
		esperado.add(new Arista(0, 1, 5));
		esperado.add(new Arista(0, 4, 9));
		return esperado;
	}

	private GrafoConPesos grafoMismosPesos() {
		GrafoConPesos grafo = new GrafoConPesos(5);
		grafo.agregarArista(0, 1, 5);
		grafo.agregarArista(0, 4, 5);
		grafo.agregarArista(1, 2, 5);
		grafo.agregarArista(2, 3, 5);
		grafo.agregarArista(4, 3, 5);
		return grafo;
	}
}