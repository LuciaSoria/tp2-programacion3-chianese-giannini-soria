package test;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import grafos.Arista;
import grafos.GrafoConPesos;

public class GrafoConPesosTest {

	private GrafoConPesos _grafo;

	@Before
	public void inicializar() {
		_grafo = new GrafoConPesos(3);
	}

	@Test
	public void agregarArista() {
		_grafo.agregarArista(0, 1, 3);

		assertTrue(_grafo.existeArista(0, 1));
	}

	@Test
	public void agregarMismaAristaConOtroPeso() {
		_grafo.agregarArista(0, 1, 3);
		_grafo.agregarArista(0, 1, 2);
		double esperado = 2;

		assertTrue(esperado == _grafo.obtenerPeso(0, 1));
	}

	@Test
	public void obtenerPeso() {
		_grafo.agregarArista(0, 2, 3);
		_grafo.agregarArista(0, 1, 2);
		_grafo.agregarArista(1, 2, 5);
		double esperado = 2;

		assertTrue(esperado == _grafo.obtenerPeso(0, 1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void obtenerPesoAristaInexistente() {
		_grafo.agregarArista(0, 2, 3);
		_grafo.agregarArista(1, 2, 5);

		_grafo.obtenerPeso(0, 1);
	}

	@Test
	public void aristas() {
		agregarAristas(_grafo);
		List<Arista> esperado = generarEsperado();

		assertEquals(esperado, _grafo.getAristas());
	}

	@Test
	public void equals() {
		GrafoConPesos grafoEquals = new GrafoConPesos(3);

		iniciarGrafos(_grafo, grafoEquals);

		assertEquals(_grafo, grafoEquals);
	}

	@Test
	public void notEquals() {
		GrafoConPesos grafoNotEquals = new GrafoConPesos(3);

		iniciarGrafos(_grafo, grafoNotEquals);
		grafoNotEquals.agregarArista(1, 2, 4);

		assertNotEquals(_grafo, grafoNotEquals);
	}

	private void agregarAristas(GrafoConPesos grafo) {
		grafo.agregarArista(0, 1, 3);
		grafo.agregarArista(0, 2, 5);
		grafo.agregarArista(1, 2, 2);
	}

	private List<Arista> generarEsperado() {
		List<Arista> esperado = new ArrayList<>();
		Arista rel = new Arista(0, 1, 3);
		Arista rel2 = new Arista(0, 2, 5);
		Arista rel3 = new Arista(1, 2, 2);

		esperado.add(rel);
		esperado.add(rel2);
		esperado.add(rel3);

		return esperado;
	}

	private void iniciarGrafos(GrafoConPesos grafo, GrafoConPesos grafoEquals) {
		grafo.agregarArista(0, 1, 2);
		grafo.agregarArista(1, 2, 1.5);

		grafoEquals.agregarArista(0, 1, 2);
		grafoEquals.agregarArista(2, 1, 1.5);
	}
}