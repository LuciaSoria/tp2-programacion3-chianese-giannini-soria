package test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import grafos.Grafo;

public class GrafoTest {
	
private Grafo grafo;
	
	@Before
	public void inicializar() {
		grafo = new Grafo(3);
	}

	@Test
	public void agregarVertice() {
		grafo.agregarVertice();
		
		assertEquals(4, grafo.tamano());
	}

	@Test(expected = IllegalArgumentException.class)
	public void grafoConPocosVertices() {
		Grafo grafo = new Grafo(-1);
	}

	@Test
	public void agregarArista() {
		grafo.agregarArista(0, 1);

		assertTrue(grafo.existeArista(0, 1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaAlMismoVertice() {
		grafo.agregarArista(0, 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaAUnVerticeInvalido() {
		grafo.agregarArista(0, 3);
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaVerticeNegativo() {
		grafo.agregarArista(0, -3);
	}

	@Test
	public void existeAristaInversa() {
		grafo.agregarArista(1, 2);

		assertTrue(grafo.existeArista(2, 1));
	}

	@Test
	public void noExisteArista() {
		assertFalse(grafo.existeArista(0, 1));
	}

	@Test
	public void existeAristaAlMismoVertice() {
		assertFalse(grafo.existeArista(0, 0));
	}

	@Test
	public void existeAristaAUnVerticeInvalido() {
		assertFalse(grafo.existeArista(0, 3));
	}

	@Test(expected = IllegalArgumentException.class)
	public void existeAristaVerticeNegativo() {
		grafo.existeArista(0, -3);
	}

	@Test
	public void eliminarArista() {
		grafo.agregarArista(0, 2);
		grafo.eliminarArista(0, 2);

		assertFalse(grafo.existeArista(0, 2));
	}

	@Test
	public void eliminarAristaInversa() {
		grafo.agregarArista(0, 2);
		grafo.eliminarArista(2, 0);

		assertFalse(grafo.existeArista(0, 2));
	}

	@Test
	public void eliminarAristaInexistente() {
		grafo.eliminarArista(2, 0);

		assertFalse(grafo.existeArista(2, 0));
	}

	@Test
	public void eliminarAristaAUnMismoVertice() {
		grafo.eliminarArista(0, 0);

		assertFalse(grafo.existeArista(0, 0));
	}

	@Test
	public void eliminarAristaAUnVerticeInvalido() {
		grafo.eliminarArista(0, 3);
	}

	@Test(expected = IllegalArgumentException.class)
	public void eliminarAristaVerticeNegativo() {
		grafo.eliminarArista(-2, 0);
	}

	@Test
	public void eliminarPrimeraArista() {
		grafo.agregarArista(0, 1);
		grafo.eliminarArista(0, 1);

		assertFalse(grafo.existeArista(0, 1));
	}

	@Test
	public void tamano() {
		int esperado = 3;

		assertEquals(esperado, grafo.tamano());
	}

	@Test
	public void vecinos() {
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(1, 2);

		Set<Integer> esperado = new HashSet<>();
		esperado.add(1);
		esperado.add(2);

		assertEquals(esperado, grafo.vecinos(0));
	}

	@Test
	public void sinVecinos() {
		Set<Integer> esperado = new HashSet<>();

		assertEquals(esperado, grafo.vecinos(0));
	}

	@Test(expected = IllegalArgumentException.class)
	public void vecinosInvalido() {
		grafo.vecinos(3);
	}

	@Test
	public void equals() {
		Grafo grafoEquals = new Grafo(3);
		iniciarGrafos(grafo, grafoEquals);

		assertEquals(grafo, grafoEquals);
	}

	@Test
	public void notEquals() {
		Grafo grafoNotEquals = new Grafo(3);

		iniciarGrafos(grafo, grafoNotEquals);
		grafo.agregarArista(2, 0);

		assertNotEquals(grafo, grafoNotEquals);
	}

	private void iniciarGrafos(Grafo grafo, Grafo grafoEquals) {
		grafo.agregarArista(0, 1);
		grafo.agregarArista(1, 2);

		grafoEquals.agregarArista(0, 1);
		grafoEquals.agregarArista(1, 2);
	}
}
