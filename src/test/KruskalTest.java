package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import arbolGeneradorMinimo.Kruskal;
import grafos.GenerarGrafos;
import grafos.GrafoConPesos;

public class KruskalTest {

	private GrafoConPesos grafo;
	private GrafoConPesos esperado;

	@Before
	public void iniciar() {
		grafo = new GrafoConPesos();
		esperado = new GrafoConPesos();
	}

	@Test(expected = IllegalArgumentException.class)
	public void grafoNulo() {
		Kruskal kruskal = new Kruskal(null);
		kruskal.arbolGeneradorMinimo();
	}

	@Test
	public void grafoVacio() {
		Kruskal kruskal = new Kruskal(grafo);
		kruskal.arbolGeneradorMinimo();

		assertEquals(esperado, grafo);
	}

	@Test
	public void arbolGeneradorMinimoPrimerTest() {
		grafo = GenerarGrafos.generarGrafo1();
		esperado = GenerarGrafos.resultadoEsperadoGrafo1();
		Kruskal kruskal = new Kruskal(grafo);

		assertEquals(esperado, kruskal.arbolGeneradorMinimo());
	}

	@Test
	public void arbolGeneradorMinimoSegundoTest() {
		grafo = GenerarGrafos.generarGrafo2();
		esperado = GenerarGrafos.resultadoEsperadoGrafo2();
		Kruskal kruskal = new Kruskal(grafo);

		assertEquals(esperado, kruskal.arbolGeneradorMinimo());
	}

	@Test
	public void arbolGeneradorMinimoTercerTest() {
		grafo = GenerarGrafos.generarGrafo3();
		esperado = GenerarGrafos.resultadoEsperadoGrafo3();
		Kruskal kruskal = new Kruskal(grafo);

		assertEquals(esperado, kruskal.arbolGeneradorMinimo());
	}

	@Test
	public void arbolGeneradorMinimoCuartoTest() {
		grafo = GenerarGrafos.generarGrafo4();
		esperado = GenerarGrafos.resultadoEsperadoGrafo4();
		Kruskal kruskal = new Kruskal(grafo);

		assertEquals(esperado, kruskal.arbolGeneradorMinimo());
	}

	@Test
	public void arbolGeneradorMinimoQuintoTest() {
		grafo = GenerarGrafos.generarGrafo5();
		esperado = GenerarGrafos.resultadoEsperadoGrafo5();
		Kruskal kruskal = new Kruskal(grafo);

		assertEquals(esperado, kruskal.arbolGeneradorMinimo());
	}
}