package arbolGeneradorMinimo;

import java.util.ArrayList;
import java.util.List;

import grafos.Arista;
import grafos.GrafoConPesos;

public class Prim {
	private List<Integer> _verticesRecorridos;
	private List<Arista> _aristasElegidas;
	private GrafoConPesos _grafo;

	public Prim(GrafoConPesos grafo) {
		if (grafo == null)
			throw new IllegalArgumentException("El grafo no puede ser nulo");
		if (!BFS.esConexo(grafo))
			throw new IllegalArgumentException("El grafo debe ser conexo.");

		_grafo = grafo;
	}

	public GrafoConPesos arbolGeneradorMinimo() {
		if (_grafo.tamano() == 0)
			return _grafo;

		int cantidadDeVertices = _grafo.tamano();
		_verticesRecorridos = new ArrayList<Integer>(cantidadDeVertices);
		_aristasElegidas = new ArrayList<Arista>(cantidadDeVertices - 1);

		_verticesRecorridos.add(0);

		determinarAristas();
		GrafoConPesos arbolGeneradorMinimo = crearArbolGeneradorMinimo(cantidadDeVertices);
		return arbolGeneradorMinimo;
	}

	private void determinarAristas() {
		int i = 1;
		while (i < _grafo.tamano()) {
			Arista aristaElegida = elegirArista();
			Integer vertice1 = aristaElegida.getVertice1();
			Integer vertice2 = aristaElegida.getVertice2();

			_aristasElegidas.add(aristaElegida);
			if (_verticesRecorridos.contains(vertice1))
				_verticesRecorridos.add(vertice2);
			else
				_verticesRecorridos.add(vertice1);
			i++;
		}
	}

	private Arista elegirArista() {
		List<Arista> aristasPosibles = AuxiliaresPrim.aristasPosibles(_grafo, _verticesRecorridos);
		Arista elegida = AuxiliaresPrim.aristaDeMenorPeso(aristasPosibles);
		return elegida;
	}

	private GrafoConPesos crearArbolGeneradorMinimo(int cantidadDeVertices) {
		GrafoConPesos grafo = new GrafoConPesos(cantidadDeVertices);

		for (Arista arista : _aristasElegidas) {
			grafo.agregarArista(arista.getVertice1(), arista.getVertice2(), arista.getPeso());
		}
		return grafo;
	}
}
