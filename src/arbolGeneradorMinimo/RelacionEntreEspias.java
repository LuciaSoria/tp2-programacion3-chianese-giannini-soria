package arbolGeneradorMinimo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;

import grafos.Arista;
import grafos.GrafoConPesos;

public class RelacionEntreEspias {

	private List<String> _espias;
	private GrafoConPesos _relacion;

	public RelacionEntreEspias() {
		_espias = new ArrayList<>();
		_relacion = new GrafoConPesos();
	}

	public void agregarEspia(String espia) {
		nombreValido(espia);
		comprobarQueNoExista(espia);
		_espias.add(espia);
		_relacion.agregarVertice();
	}

	private void nombreValido(String espia) throws IllegalArgumentException {
		if (espia.length() <= 0)
			throw new IllegalArgumentException("El nombre del espia no puede ser vacio.");
	}

	private void comprobarQueNoExista(String espia) throws IllegalArgumentException {
		if (_espias.contains(espia))
			throw new IllegalArgumentException("El espia ingresado ya fue registrado: " + espia);
	}

	public void agregarEncuentro(String espia1, String espia2, double riesgo) {
		comprobarQueNoSonIguales(espia1, espia2);
		comprobarQueExista(espia1);
		comprobarQueExista(espia2);
		comprobarRiesgoValido(riesgo);

		int vertice1 = _espias.indexOf(espia1);
		int vertice2 = _espias.indexOf(espia2);

		_relacion.agregarArista(vertice1, vertice2, riesgo);
	}

	private void comprobarQueNoSonIguales(String espia1, String espia2) throws IllegalArgumentException {
		if (espia1.equals(espia2))
			throw new IllegalArgumentException("Un espia no puede encontrarse consigo mismo");
	}

	private void comprobarQueExista(String espia) throws IllegalArgumentException {
		if (!_espias.contains(espia))
			throw new IllegalArgumentException("El espia ingresado no fue registrado: " + espia);
	}
	
	public boolean comprobarRiesgoValido(Double riesgo) {
		return riesgo >= 0 && riesgo <= 1;
	}

	public RelacionEntreEspias hallarElArbolGeneradorMinimoPrim() {
		RelacionEntreEspias nuevaRelacion = new RelacionEntreEspias();
		GrafoConPesos nuevo = new GrafoConPesos();

		Prim prim = new Prim(_relacion);
		nuevo = prim.arbolGeneradorMinimo();
		nuevaRelacion._espias = this._espias;
		nuevaRelacion._relacion = nuevo;
		return nuevaRelacion;
	}

	public RelacionEntreEspias hallarElArbolGeneradorMinimoKruskal() {
		RelacionEntreEspias nuevaRelacion = new RelacionEntreEspias();
		GrafoConPesos nuevo = new GrafoConPesos();

		Kruskal kruskal = new Kruskal(_relacion);
		nuevo = kruskal.arbolGeneradorMinimo();
		nuevaRelacion._espias = this._espias;
		nuevaRelacion._relacion = nuevo;
		return nuevaRelacion;
	}

	public Set<List<String>> relaciones() {
		List<Arista> relacionesGrafo = _relacion.getAristas();
		Set<List<String>> relacionesEspias = new HashSet<>();

		for (Arista relacionGrafo : relacionesGrafo) {
			List<String> relacion = new ArrayList<>(3);
			int posicionDelNinja1 = relacionGrafo.getVertice1();
			int posicionDelNinja2 = relacionGrafo.getVertice2();
			relacion.add(0, espiaDeLaPosicion(posicionDelNinja1));
			relacion.add(1, espiaDeLaPosicion(posicionDelNinja2));
			relacion.add(2, "" + relacionGrafo.getPeso());
			relacionesEspias.add(relacion);
		}
		return relacionesEspias;
	}

	public boolean siEsConexo() {
		return BFS.esConexo(_relacion);
	}

	private String espiaDeLaPosicion(int posicion) {
		return _espias.get(posicion);
	}

	public List<String> getNombres() {
		return _espias;
	}

	public boolean existeEspia(String espia) {
		return _espias.contains(espia);
	}

	public boolean existeEncuentro(String espia1, String espia2) {
		int vertice1 = _espias.indexOf(espia1);
		int vertice2 = _espias.indexOf(espia2);
		return _relacion.existeArista(vertice1, vertice2);
	}
}