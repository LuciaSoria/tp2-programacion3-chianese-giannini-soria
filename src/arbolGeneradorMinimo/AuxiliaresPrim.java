package arbolGeneradorMinimo;

import java.util.ArrayList;
import java.util.List;

import grafos.Arista;
import grafos.GrafoConPesos;

public class AuxiliaresPrim {

	public static List<Arista> aristasPosibles(GrafoConPesos grafo, List<Integer> verticesRecorridos) {
		comprobarQueNoSeaNull(grafo);
		comprobarQueNoSeaNull(verticesRecorridos);
		List<Arista> aristas = new ArrayList<>();

		for (Arista arista : grafo.getAristas()) {
			Integer vertice1 = arista.getVertice1();
			Integer vertice2 = arista.getVertice2();
			if ((verticesRecorridos.contains(vertice1) && !verticesRecorridos.contains(vertice2))
					|| (verticesRecorridos.contains(vertice2) && !verticesRecorridos.contains(vertice1))) {
				aristas.add(arista);
				if (arista.getPeso() < aristas.get(0).getPeso()) {
					aristas.remove(aristas.get(aristas.size() - 1));
					aristas.add(0, arista);
				}
			}
		}
		return aristas;
	}

	public static Arista aristaDeMenorPeso(List<Arista> aristas) {
		comprobarQueNoSeaNull(aristas);
		Arista aristaDeMenorPeso = aristas.get(0);
		return aristaDeMenorPeso;
	}

	private static void comprobarQueNoSeaNull(Object ob) throws IllegalArgumentException {
		if (ob == null)
			throw new IllegalArgumentException("el parametro no puede ser nulo.");
	}
}