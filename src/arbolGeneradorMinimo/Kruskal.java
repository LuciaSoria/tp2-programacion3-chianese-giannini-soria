package arbolGeneradorMinimo;

import java.util.List;

import grafos.Arista;
import grafos.GrafoConPesos;

public class Kruskal {

	private GrafoConPesos _grafo;
	private List<Arista> _aristasOriginales;
	private GrafoConPesos _nuevoGrafo;

	public Kruskal(GrafoConPesos grafo) {
		if (grafo == null)
			throw new IllegalArgumentException("El grafo no puede ser nulo.");
		if (!BFS.esConexo(grafo))
			throw new IllegalArgumentException("El grafo debe ser conexo.");

		_grafo = grafo;
		_aristasOriginales = grafo.getAristas();
		_nuevoGrafo = new GrafoConPesos(grafo.tamano());
	}

	public GrafoConPesos arbolGeneradorMinimo() {
		crearArbolGeneradorMinimo();
		return _nuevoGrafo;
	}

	private void crearArbolGeneradorMinimo() {
		int cantidadDeVertices = _grafo.tamano();
		int i = 1;
		while (i < cantidadDeVertices) {
			Arista elegida = AuxiliaresKruskal.elegirAristaMinima(_aristasOriginales, _nuevoGrafo);
			_aristasOriginales.remove(elegida);
			agregarArista(elegida);
			i++;
		}
	}

	private void agregarArista(Arista arista) {
		int vertice1 = arista.getVertice1();
		int vertice2 = arista.getVertice2();
		double peso = arista.getPeso();

		_nuevoGrafo.agregarArista(vertice1, vertice2, peso);
	}
}
