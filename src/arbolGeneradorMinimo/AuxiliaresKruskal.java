package arbolGeneradorMinimo;

import java.util.ArrayList;
import java.util.List;

import grafos.Arista;
import grafos.Grafo;
import grafos.GrafoConPesos;

public class AuxiliaresKruskal {

	public static boolean generaCiclo(GrafoConPesos grafo, Arista arista) {
		comprobarQueNoSeaNull(grafo);
		comprobarQueNoSeaNull(arista);

		if (grafo.getAristas().size() == 0)
			return false;
		Integer vertice1 = arista.getVertice1();
		Integer vertice2 = arista.getVertice2();

		comprobarQueSeaValido(grafo, vertice1);
		comprobarQueSeaValido(grafo, vertice2);

		boolean generaCiclo = BFS.alcanzables(grafo, vertice1).contains(vertice2);
		return generaCiclo;
	}

	public static Arista elegirAristaMinima(List<Arista> aristasOriginales, GrafoConPesos nuevoGrafo) {
		comprobarQueNoSeaNull(aristasOriginales);
		comprobarQueNoSeaNull(nuevoGrafo);
		comprobarQueNoSeaVacia(aristasOriginales);

		List<Arista> aristasQueNoGeneranCiclo = new ArrayList<Arista>();
		for (Arista arista : aristasOriginales) {
			if (!AuxiliaresKruskal.generaCiclo(nuevoGrafo, arista)) {
				aristasQueNoGeneranCiclo.add(arista);
				if (arista.getPeso() < aristasQueNoGeneranCiclo.get(0).getPeso()) {
					aristasQueNoGeneranCiclo.remove(aristasQueNoGeneranCiclo.get(aristasQueNoGeneranCiclo.size() - 1));
					aristasQueNoGeneranCiclo.add(0, arista);
				}
			}
		}

		Arista aristaDeMenorPeso = aristasQueNoGeneranCiclo.get(0);
		return aristaDeMenorPeso;
	}

	private static void comprobarQueNoSeaNull(Object ob) throws IllegalArgumentException {
		if (ob == null)
			throw new IllegalArgumentException("el parametro no puede ser nulo.");
	}

	private static void comprobarQueSeaValido(Grafo grafo, int vertice) throws IllegalArgumentException {
		if (vertice < 0 || vertice >= grafo.tamano()) {
			throw new IllegalArgumentException("Vertice invalido: " + vertice);
		}
	}

	private static void comprobarQueNoSeaVacia(List<Arista> lista) throws IllegalArgumentException {
		if (lista.size() == 0)
			throw new IllegalArgumentException("La lista no puede ser vacia.");
	}
}