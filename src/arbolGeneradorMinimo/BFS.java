package arbolGeneradorMinimo;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import grafos.Grafo;

public class BFS {

	private static List<Integer> vertices;
	private static boolean[] marcados;

	public static boolean esConexo(Grafo grafo) {
		comprobarQueNoSeaNull(grafo);
		if (grafo.tamano() == 0)
			return true;
		return alcanzables(grafo, 0).size() == grafo.tamano();
	}

	public static Set<Integer> alcanzables(Grafo grafo, int vertice) {
		comprobarQueNoSeaNull(grafo);
		comprobarQueSeaValido(grafo, vertice);

		Set<Integer> alcanzables = new HashSet<Integer>();
		vertices = new LinkedList<Integer>();
		marcados = new boolean[grafo.tamano()];
		vertices.add(vertice);

		while (vertices.size() > 0) {
			int i = seleccionarYMarcarVertice(alcanzables);
			agregarVecinosNoMarcados(grafo, i);
			vertices.remove(0);
		}
		return alcanzables;
	}

	private static int seleccionarYMarcarVertice(Set<Integer> alcanzables) {
		int i = vertices.get(0);
		marcados[i] = true;
		alcanzables.add(i);
		return i;
	}

	private static void agregarVecinosNoMarcados(Grafo grafo, int vertice) {
		for (int vecino : grafo.vecinos(vertice)) {
			if (!marcados[vecino] && !vertices.contains(vecino))
				vertices.add(vecino);
		}
	}

	private static void comprobarQueNoSeaNull(Object ob) throws IllegalArgumentException {
		if (ob == null)
			throw new IllegalArgumentException("el parametro no puede ser nulo.");
	}

	private static void comprobarQueSeaValido(Grafo grafo, int vertice) throws IllegalArgumentException {
		if (vertice < 0 || vertice >= grafo.tamano()) {
			throw new IllegalArgumentException("Vertice invalido: " + vertice);
		}
	}
}