package grafos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Grafo {

	private List<Set<Integer>> _vecinos;

	public Grafo(int vertices) {
		if (vertices < 0) {
			throw new IllegalArgumentException("La cantidad de vertices no puede ser negativa: " + vertices);
		}
		_vecinos = new ArrayList<>(vertices);
		for (int i = 0; i < vertices; i++) {
			_vecinos.add(i, new HashSet<Integer>());
		}
	}

	public Grafo() {
		_vecinos = new ArrayList<>();
	}

	public void agregarVertice() {
		_vecinos.add(new HashSet<Integer>());
	}

	public void agregarArista(int vertice1, int vertice2) {
		comprobarQueVerticeEsValido(vertice1);
		comprobarQueVerticeEsValido(vertice2);
		comprobarQueNoSonIguales(vertice1, vertice2);

		_vecinos.get(vertice1).add(vertice2);
		_vecinos.get(vertice2).add(vertice1);
	}

	public boolean existeArista(int vertice1, int vertice2) {
		if (tamano() < 2)
			return false;
		comprobarVerticePositivo(vertice1);
		comprobarVerticePositivo(vertice2);
		return _vecinos.get(vertice1).contains(vertice2);
	}

	public void eliminarArista(int vertice1, int vertice2) {
		comprobarVerticePositivo(vertice1);
		comprobarVerticePositivo(vertice2);
		if (!existeArista(vertice1, vertice2))
			return;
		_vecinos.get(vertice1).remove(vertice2);
		_vecinos.get(vertice2).remove(vertice1);
	}

	public int tamano() {
		return _vecinos.size();
	}

	public Set<Integer> vecinos(int vertice) {
		comprobarQueVerticeEsValido(vertice);
		return _vecinos.get(vertice);
	}

	public List<Set<Integer>> getVecinos() {
		return _vecinos;
	}

	private void comprobarQueVerticeEsValido(int vertice) throws IllegalArgumentException {
		comprobarQueExisteVertice(vertice);
		comprobarVerticePositivo(vertice);
	}

	private void comprobarQueExisteVertice(int vertice) throws IllegalArgumentException {
		if (vertice >= _vecinos.size())
			throw new IllegalArgumentException("Vertice inexistente: " + vertice);
	}

	private void comprobarVerticePositivo(int vertice) throws IllegalArgumentException {
		if (vertice < 0)
			throw new IllegalArgumentException("Vertice negativo: " + vertice);
	}

	private void comprobarQueNoSonIguales(int vertice1, int vertice2) throws IllegalArgumentException {
		if (vertice1 == vertice2)
			throw new IllegalArgumentException(
					"No puede haber una arista al mismo vertice: " + vertice1 + " = " + vertice2);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_vecinos == null) ? 0 : _vecinos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grafo other = (Grafo) obj;
		if (getVecinos() == null)
			if (other.getVecinos() != null)
				return false;
		if (getVecinos().size() != other.getVecinos().size()) {
			return false;
		}
		for (int i = 0; i < getVecinos().size(); i++) {
			if (!getVecinos().get(i).equals(other.getVecinos().get(i)))
				return false;
		}
		return true;
	}
}
