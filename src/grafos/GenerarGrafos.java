package grafos;

import java.util.List;

public class GenerarGrafos {

	public static void cargarTodosLosGrafos(List<GrafoConPesos> grafos) {
		grafos.add(GenerarGrafos.generarGrafo1());
		grafos.add(GenerarGrafos.generarGrafo2());
		grafos.add(GenerarGrafos.generarGrafo3());
		grafos.add(GenerarGrafos.generarGrafo4());
		grafos.add(GenerarGrafos.generarGrafo5());
	}

	public static GrafoConPesos generarGrafo1() {
		GrafoConPesos grafo = new GrafoConPesos(6);
		grafo.agregarArista(0, 1, 6);
		grafo.agregarArista(0, 3, 5);
		grafo.agregarArista(0, 2, 1);
		grafo.agregarArista(1, 2, 5);
		grafo.agregarArista(1, 4, 3);
		grafo.agregarArista(2, 3, 5);
		grafo.agregarArista(2, 5, 4);
		grafo.agregarArista(2, 4, 6);
		grafo.agregarArista(4, 5, 6);
		grafo.agregarArista(5, 3, 2);
		return grafo;
	}

	public static GrafoConPesos resultadoEsperadoGrafo1() {
		GrafoConPesos esperado = new GrafoConPesos(6);
		esperado.agregarArista(0, 2, 1);
		esperado.agregarArista(1, 2, 5);
		esperado.agregarArista(1, 4, 3);
		esperado.agregarArista(3, 5, 2);
		esperado.agregarArista(2, 5, 4);
		return esperado;
	}

	public static GrafoConPesos generarGrafo2() {
		GrafoConPesos grafo = new GrafoConPesos(5);
		grafo.agregarArista(0, 1, 2);
		grafo.agregarArista(0, 2, 5);
		grafo.agregarArista(0, 4, 6);
		grafo.agregarArista(1, 2, 5);
		grafo.agregarArista(1, 3, 3);
		grafo.agregarArista(1, 4, 2);
		grafo.agregarArista(2, 4, 4);
		grafo.agregarArista(3, 4, 4);
		return grafo;
	}

	public static GrafoConPesos resultadoEsperadoGrafo2() {
		GrafoConPesos esperado = new GrafoConPesos(5);
		esperado.agregarArista(0, 1, 2);
		esperado.agregarArista(1, 4, 2);
		esperado.agregarArista(1, 3, 3);
		esperado.agregarArista(4, 2, 4);
		return esperado;
	}

	public static GrafoConPesos generarGrafo3() {
		GrafoConPesos grafo = new GrafoConPesos(7);
		grafo.agregarArista(0, 1, 7);
		grafo.agregarArista(0, 3, 5);
		grafo.agregarArista(1, 2, 8);
		grafo.agregarArista(1, 4, 7);
		grafo.agregarArista(2, 4, 5);
		grafo.agregarArista(1, 3, 9);
		grafo.agregarArista(3, 4, 15);
		grafo.agregarArista(3, 5, 6);
		grafo.agregarArista(4, 5, 8);
		grafo.agregarArista(4, 6, 9);
		grafo.agregarArista(5, 6, 11);
		return grafo;
	}

	public static GrafoConPesos resultadoEsperadoGrafo3() {
		GrafoConPesos esperado = new GrafoConPesos(7);
		esperado.agregarArista(0, 1, 7);
		esperado.agregarArista(1, 4, 7);
		esperado.agregarArista(4, 2, 5);
		esperado.agregarArista(0, 3, 5);
		esperado.agregarArista(3, 5, 6);
		esperado.agregarArista(4, 6, 9);
		return esperado;
	}

	public static GrafoConPesos generarGrafo4() {
		GrafoConPesos grafo = new GrafoConPesos(9);
		grafo.agregarArista(0, 1, 14);
		grafo.agregarArista(0, 2, 5);
		grafo.agregarArista(0, 3, 2);
		grafo.agregarArista(1, 2, 9);
		grafo.agregarArista(1, 3, 8);
		grafo.agregarArista(1, 4, 15);
		grafo.agregarArista(4, 3, 10);
		grafo.agregarArista(4, 2, 13);
		grafo.agregarArista(4, 6, 7);
		grafo.agregarArista(5, 4, 1);
		grafo.agregarArista(5, 2, 8);
		grafo.agregarArista(5, 6, 10);
		grafo.agregarArista(4, 7, 5);
		grafo.agregarArista(6, 7, 0);
		grafo.agregarArista(8, 7, 6);
		grafo.agregarArista(8, 5, 11);
		grafo.agregarArista(8, 6, 12);
		grafo.agregarArista(1, 4, 15);
		return grafo;
	}

	public static GrafoConPesos resultadoEsperadoGrafo4() {
		GrafoConPesos esperado = new GrafoConPesos(9);
		esperado.agregarArista(1, 3, 8);
		esperado.agregarArista(3, 0, 2);
		esperado.agregarArista(0, 2, 5);
		esperado.agregarArista(2, 5, 8);
		esperado.agregarArista(5, 4, 1);
		esperado.agregarArista(4, 7, 5);
		esperado.agregarArista(7, 6, 0);
		esperado.agregarArista(7, 8, 6);
		return esperado;
	}

	public static GrafoConPesos generarGrafo5() {
		GrafoConPesos grafo = new GrafoConPesos(9);
		grafo.agregarArista(0, 1, 20);
		grafo.agregarArista(0, 3, 35);
		grafo.agregarArista(0, 4, 30);
		grafo.agregarArista(1, 2, 7);
		grafo.agregarArista(1, 5, 9);
		grafo.agregarArista(1, 4, 18);
		grafo.agregarArista(2, 5, 20);
		grafo.agregarArista(5, 4, 11);
		grafo.agregarArista(5, 8, 4);
		grafo.agregarArista(4, 8, 25);
		grafo.agregarArista(4, 7, 15);
		grafo.agregarArista(4, 6, 12);
		grafo.agregarArista(4, 3, 23);
		grafo.agregarArista(3, 6, 25);
		grafo.agregarArista(6, 7, 22);
		grafo.agregarArista(7, 8, 20);
		return grafo;
	}

	public static GrafoConPesos resultadoEsperadoGrafo5() {
		GrafoConPesos esperado = new GrafoConPesos(9);
		esperado.agregarArista(0, 1, 20);
		esperado.agregarArista(1, 2, 7);
		esperado.agregarArista(1, 5, 9);
		esperado.agregarArista(5, 8, 4);
		esperado.agregarArista(4, 5, 11);
		esperado.agregarArista(4, 7, 15);
		esperado.agregarArista(4, 6, 12);
		esperado.agregarArista(4, 3, 23);
		return esperado;
	}
}