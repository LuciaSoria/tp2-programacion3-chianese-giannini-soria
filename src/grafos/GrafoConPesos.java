package grafos;

import java.util.ArrayList;
import java.util.List;

public class GrafoConPesos extends Grafo {
	private List<Arista> _aristas;

	public GrafoConPesos(int vertices) {
		super(vertices);
		_aristas = new ArrayList<>();
	}

	public GrafoConPesos() {
		super();
		_aristas = new ArrayList<>();
	}

	public void agregarArista(int vertice1, int vertice2, double peso) {
		if (super.existeArista(vertice1, vertice2)) {
			cambiarPeso(vertice1, vertice2, peso);
		} else {
			super.agregarArista(vertice1, vertice2);
			_aristas.add(new Arista(vertice1, vertice2, peso));
		}
	}

	private void cambiarPeso(int vertice1, int vertice2, double peso) {
		for (Arista arista : _aristas) {
			if ((arista.getVertice1() == vertice1 && arista.getVertice2() == vertice2)
					|| (arista.getVertice1() == vertice2 && arista.getVertice2() == vertice1)) {
				arista.setPeso(peso);
			}
		}
	}

	public double obtenerPeso(int vertice1, int vertice2) throws IllegalArgumentException {
		if (!super.existeArista(vertice1, vertice2)) {
			throw new IllegalArgumentException("No existe arista entre " + vertice1 + " y " + vertice2);
		}
		double peso = 0;
		for (Arista arista : _aristas) {
			if ((arista.getVertice1() == vertice1 && arista.getVertice2() == vertice2)
					|| (arista.getVertice1() == vertice2 && arista.getVertice2() == vertice1)) {
				peso = arista.getPeso();
			}
		}
		return peso;
	}

	public List<Arista> getAristas() {
		return _aristas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_aristas == null) ? 0 : _aristas.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		if (!super.equals(obj)) {
			return false;
		}
		GrafoConPesos other = (GrafoConPesos) obj;
		if (getAristas() == null)
			if (other.getAristas() != null)
				return false;
		if (getAristas().size() != other.getAristas().size()) {
			return false;
		}
		for (Arista arista : getAristas()) {
			if (!other.getAristas().contains(arista))
				return false;
		}
		return true;
	}
}
