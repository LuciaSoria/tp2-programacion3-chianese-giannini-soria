package grafos;

public class Arista {
	private int _vertice1;
	private int _vertice2;
	private double _peso;

	public Arista(int vertice1, int vertice2, double peso) {
		_vertice1 = vertice1;
		_vertice2 = vertice2;
		_peso = peso;
	}

	public int getVertice1() {
		return _vertice1;
	}

	public int getVertice2() {
		return _vertice2;
	}

	public double getPeso() {
		return _peso;
	}

	public void setPeso(double peso) {
		_peso = peso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + _peso);
		result = prime * result + _vertice1;
		result = prime * result + _vertice2;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arista other = (Arista) obj;
		if (_peso != other._peso)
			return false;
		if (_vertice1 == other._vertice1 && _vertice2 == other._vertice2)
			return true;
		if (_vertice1 == other._vertice2 && _vertice2 == other._vertice1)
			return true;
		return false;
	}
}