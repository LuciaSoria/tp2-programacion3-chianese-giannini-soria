package interfaz;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import arbolGeneradorMinimo.RelacionEntreEspias;

@SuppressWarnings("serial")
public class Principal extends JFrame {

	private MenuEspias _menuEspia;
	private MenuConexiones _menuConexion;
	private MenuResultado _menuResultado;
	private RelacionEntreEspias _espias;
	

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Principal() {
		_menuEspia = new MenuEspias();
		_menuConexion = new MenuConexiones();
		_menuResultado = new MenuResultado();
		_espias = new RelacionEntreEspias();

		menuEspias();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 350);
	}

	public void menuEspias() {
		getContentPane().revalidate();
		setContentPane(_menuEspia);
		_menuEspia.setRelacionEntreEspias(_espias);

		JButton botonAgregarArista = new JButton("Menu Conexiones");
		botonAgregarArista.setFont(new Font("Tahoma", Font.PLAIN, 15));
		botonAgregarArista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuConexiones();
			}
		});
		botonAgregarArista.setBounds(370, 11, 160, 23);
		getContentPane().add(botonAgregarArista);
	}

	public void menuConexiones() {
		_menuConexion.setRelacionEntreEspias(_espias);
		_menuConexion.cargarNombresEspias();

		getContentPane().revalidate();
		setContentPane(_menuConexion);

		JButton botonAgregarEspia = new JButton("Menu Espias");
		botonAgregarEspia.setFont(new Font("Tahoma", Font.PLAIN, 15));
		botonAgregarEspia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_menuConexion.limpiarComboBoxes();
				menuEspias();
			}
		});
		botonAgregarEspia.setBounds(69, 11, 160, 23);
		getContentPane().add(botonAgregarEspia);

		JButton botonResultado = new JButton("Resultado");
		botonResultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (_espias.siEsConexo() && _espias.getNombres().size() > 0) {
					menuResultado();
				} else {
					JOptionPane.showMessageDialog(null,
							"Las relaciones ingresadas no son suficientes para calcular el resultado.");
				}
			}
		});
		botonResultado.setBounds(387, 113, 148, 23);
		_menuConexion.add(botonResultado);
	}

	public void menuResultado() {
		_menuResultado.setRelacionEntreEspias(_espias);

		getContentPane().revalidate();
		setContentPane(_menuResultado);
	}
}