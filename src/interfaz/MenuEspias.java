package interfaz;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import arbolGeneradorMinimo.RelacionEntreEspias;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;

public class MenuEspias extends JPanel {

	private JTextField textFieldNombreEspia;
	private RelacionEntreEspias _relacionEntreEspias;
	private JLabel jLabelNombreEspia;
	private JButton botonAgregarEspia;
	private Image _imagen;

	public MenuEspias() {
		setLayout(null);
		initialize();
	}

	private void initialize() {
		_relacionEntreEspias = new RelacionEntreEspias();

		jLabelNombreEspia = new JLabel("Nombre Espia :");
		jLabelNombreEspia.setFont(new Font("Tahoma", Font.PLAIN, 20));
		jLabelNombreEspia.setForeground(Color.white);
		jLabelNombreEspia.setBounds(133, 128, 163, 27);
		add(jLabelNombreEspia);

		textFieldNombreEspia = new JTextField();
		textFieldNombreEspia.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldNombreEspia.setBounds(288, 128, 172, 31);
		add(textFieldNombreEspia);
		textFieldNombreEspia.setColumns(10);

		botonAgregarEspia = new JButton("Agregar");
		botonAgregarEspia.setFont(new Font("Tahoma", Font.PLAIN, 15));
		botonAgregarEspia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nombreEspia = textFieldNombreEspia.getText();
				try {
					_relacionEntreEspias.agregarEspia(nombreEspia);
					textFieldNombreEspia.setText("");
				} catch (IllegalArgumentException exception) {
					if (nombreEspia.length() == 0)
						JOptionPane.showMessageDialog(null, "Debes ingresar el nombre del espia.");
					else
						JOptionPane.showMessageDialog(null, "El ninja ingresado ya fue registrado.");
					textFieldNombreEspia.setText("");
				}
			}
		});
		botonAgregarEspia.setBounds(249, 208, 89, 23);

		add(botonAgregarEspia);
	}

	public void setRelacionEntreEspias(RelacionEntreEspias espias) {
		this._relacionEntreEspias = espias;
	}
	
	public void paint(Graphics graphics) {
		_imagen = new ImageIcon(getClass().getResource("/interfaz/logoEspia2.jpg")).getImage();
		graphics.drawImage(_imagen, 0, 0, getWidth(), getHeight(), this);
		setOpaque(false);
		super.paint(graphics);
	}
}
