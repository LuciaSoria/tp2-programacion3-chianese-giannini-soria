package interfaz;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import arbolGeneradorMinimo.RelacionEntreEspias;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class MenuConexiones extends JPanel {

	private JTextField textFieldRiesgo;
	private RelacionEntreEspias _relacionEntreEspias;
	private JComboBox<String> comboBoxEspiaA;
	private JComboBox<String> comboBoxEspiaB;
	private Image _imagen;

	public MenuConexiones() {
		setLayout(null);
		initialize();
		agregarConexiones();
	}

	private void initialize() {
		JLabel JLabelEspiaA = new JLabel("Espia:");
		JLabelEspiaA.setFont(new Font("Tahoma", Font.PLAIN, 13));
		JLabelEspiaA.setBounds(21, 52, 46, 23);
		add(JLabelEspiaA);

		JLabel JLabelSeComunicaCon = new JLabel("Se comunica con:");
		JLabelSeComunicaCon.setFont(new Font("Tahoma", Font.PLAIN, 13));
		JLabelSeComunicaCon.setBounds(21, 100, 108, 23);
		add(JLabelSeComunicaCon);

		JLabel JLabelRiesgo = new JLabel("Riesgo:");
		JLabelRiesgo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		JLabelRiesgo.setBounds(21, 146, 55, 23);
		add(JLabelRiesgo);

		comboBoxEspiaA = new JComboBox<>();
		comboBoxEspiaA.setBounds(139, 52, 134, 22);
		add(comboBoxEspiaA);

		comboBoxEspiaB = new JComboBox<>();
		comboBoxEspiaB.setBounds(139, 100, 134, 22);
		add(comboBoxEspiaB);

		textFieldRiesgo = new JTextField();
		textFieldRiesgo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent evt) {
				char c = evt.getKeyChar();
				if (Character.isLetter(c)) {
					evt.consume();
				} else {
					try {
						Double.parseDouble(textFieldRiesgo.getText() + evt.getKeyChar());
					} catch (NumberFormatException e) {
						evt.consume();
					}
				}

			}
		});
		textFieldRiesgo.setText("0.0");
		textFieldRiesgo.setBounds(139, 149, 86, 20);
		add(textFieldRiesgo);
		textFieldRiesgo.setColumns(10);
	}

	private void agregarConexiones() {
		JButton botonAgregarConexion = new JButton("Agregar");
		botonAgregarConexion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		botonAgregarConexion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String espiaA = comboBoxEspiaA.getSelectedItem().toString();
					String espiaB = comboBoxEspiaB.getSelectedItem().toString();
					String riesgo = textFieldRiesgo.getText().length() > 0 ? textFieldRiesgo.getText() : "a";

					if (!_relacionEntreEspias.comprobarRiesgoValido(Double.parseDouble(riesgo))) {
						mostrarMensajeRiesgo();
					} else {
						Double riesgoNum = Double.parseDouble(riesgo);
						try {
							_relacionEntreEspias.agregarEncuentro(espiaA, espiaB, riesgoNum);
							textFieldRiesgo.setText("0.0");
						} catch (IllegalArgumentException exception) {
							JOptionPane.showMessageDialog(null, "Los espias deben ser distintos.");
						}
					}
				} catch (NullPointerException ex) {
					JOptionPane.showMessageDialog(null, "Debes ingresar los nombres de los espias.");
				}
			}
		});
		botonAgregarConexion.setBounds(89, 203, 89, 23);
		add(botonAgregarConexion);
	}

	public void setRelacionEntreEspias(RelacionEntreEspias _espias) {
		this._relacionEntreEspias = _espias;
	}

	public void cargarNombresEspias() {
		List<String> listaNombres = _relacionEntreEspias.getNombres();
		for (String nombre : listaNombres) {
			comboBoxEspiaA.addItem(nombre);
			comboBoxEspiaB.addItem(nombre);
		}
	}

	public void limpiarComboBoxes() {
		comboBoxEspiaA.removeAllItems();
		comboBoxEspiaB.removeAllItems();
	}

	private void mostrarMensajeRiesgo() {
		JOptionPane.showMessageDialog(null, "El riesgo debe ser un numero entre 0 y 1.");
		textFieldRiesgo.setText("");
	}

	public void paint(Graphics graphics) {
		_imagen = new ImageIcon(getClass().getResource("/interfaz/logoEspia2.jpg")).getImage();
		graphics.drawImage(_imagen, 0, 0, getWidth(), getHeight(), this);
		setOpaque(false);
		super.paint(graphics);
	}
}
