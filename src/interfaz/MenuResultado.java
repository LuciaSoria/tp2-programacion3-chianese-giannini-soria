package interfaz;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import arbolGeneradorMinimo.Prim;
import arbolGeneradorMinimo.RelacionEntreEspias;

import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Set;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

public class MenuResultado extends JPanel {
	private RelacionEntreEspias _relacionEntreEspias;
	private JTable _table;
	private JLabel textoComparacion;
	private Image _imagen;

	public MenuResultado() {
		setLayout(null);
		initialize();
	}

	private void initialize() {
		_relacionEntreEspias = new RelacionEntreEspias();

		resultadoJLabel();

		cargarTabla();

		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Espia 1");
		model.addColumn("Espia 2");
		model.addColumn("Riesgo");

		JButton botonResultado = new JButton("calcular");
		botonResultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textoComparacion.setVisible(true);
				_relacionEntreEspias = _relacionEntreEspias.hallarElArbolGeneradorMinimoPrim();
				Set<List<String>> lista = _relacionEntreEspias.relaciones();
				for (List<String> tupla : lista) {
					model.addRow(new String[] { tupla.get(0), tupla.get(1), tupla.get(2) });
				}
				botonResultado.setEnabled(false);
			}
		});

		botonResultado.setBounds(390, 19, 89, 23);
		add(botonResultado);

		_table.setModel(model);

		mostrarComparacion();
	}

	private void mostrarComparacion() {
		StringBuilder sb = new StringBuilder();
		sb.append("El algoritmo de Prim calculo el arbol generador minimo en " + tiempoPrim() + " segundos.");
		sb.append(" El de Kruskal, en " + tiempoKruskal() + " segundos.");
		String tiempo = sb.toString();
		textoComparacion = new JLabel(tiempo);
		textoComparacion.setFont(new Font("Tahoma", Font.PLAIN, 10));
		textoComparacion.setBounds(22, 287, 550, 13);
		add(textoComparacion);
		textoComparacion.setVisible(false);
	}

	private double tiempoPrim() {
		double tiempoPrim = 0;
		long inicio = System.currentTimeMillis();

		_relacionEntreEspias.hallarElArbolGeneradorMinimoPrim();

		long fin = System.currentTimeMillis();
		double tiempo = (fin - inicio) / 1000.0;

		return tiempoPrim += tiempo;
	}

	private double tiempoKruskal() {
		double tiempoKruskal = 0;
		long inicio = System.currentTimeMillis();

		_relacionEntreEspias.hallarElArbolGeneradorMinimoKruskal();

		long fin = System.currentTimeMillis();
		double tiempo = (fin - inicio) / 1000.0;

		return tiempoKruskal += tiempo;
	}

	private void resultadoJLabel() {
		JLabel textResultado = new JLabel("RESULTADO");
		textResultado.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textResultado.setBounds(170, 11, 151, 30);
		add(textResultado);
	}

	private void cargarTabla() {
		JScrollPane JS = new JScrollPane(_table);
		JS.setBounds(22, 52, 473, 237);
		this.add(JS);

		_table = new JTable();
		_table.setShowGrid(false);
		_table.setFont(new Font("Sylfaen", Font.LAYOUT_LEFT_TO_RIGHT, 16));
		_table.setGridColor(Color.LIGHT_GRAY);
		_table.setForeground(Color.BLACK);
		_table.setBorder(null);
		_table.setBounds(44, 81, 366, 208);
		_table.setRowHeight(30);
		_table.setOpaque(false);
		JS.setViewportView(_table);
	}

	public void setRelacionEntreEspias(RelacionEntreEspias espias) {
		this._relacionEntreEspias = espias;
	}
	
	public void paint(Graphics graphics) {
		_imagen = new ImageIcon(getClass().getResource("/interfaz/logoEspia2.jpg")).getImage();
		graphics.drawImage(_imagen, 0, 0, getWidth(), getHeight(), this);
		setOpaque(false);
		super.paint(graphics);
	}
}
